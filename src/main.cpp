/*
License under zlib license
Copyright (C) 2020-2022 Kevin Haryo Kuncoro

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.

Kevin Haryo Kuncoro
kevinhyokun91@gmail.com
*/

#include<iostream>
#include<SDL2/SDL.h>
#include<base_app.h>
#include<base_graphics.h>
#include<base_node.h>
#include<base_vector.h>

#include<random>

using std::cout;
using std::endl;

extern SDL_Window *window;
extern SDL_Renderer *renderer;
extern SDL_Event event;
extern int window_width;
extern int window_height;
extern int logical_screen_width;
extern int logical_screen_height;
extern long last_timer;

enum DirState{
	      right_dir,
	      left_dir,
	      up_dir,
	      down_dir,
};

struct Attribute{
  int hp;
  int mp;
  int atk;
  int def;
  int acc;
  int agi;
  float accel;
  float move_speed; 
  float hsp;
  float vsp;
  int horizontal_state;
  int vertical_state;
  int *curr_anim;
  int dir_state;
  float move_angle;
  Rectangle *hit_box;
  bool is_die;
  float texture_opacity;
  int grid_x;
  int grid_y;
};

Attribute* create_attribute(){
  Attribute *attribute = (Attribute*)malloc(sizeof(Attribute));
  attribute->hp = 3;
  attribute->dir_state = down_dir;
  attribute->move_angle = 0.0f;
  attribute->hsp = 0.0f;
  attribute->horizontal_state = 1;
  attribute->vsp = 0.0f;
  attribute->vertical_state = 1;
  attribute->accel = 0.1f;
  attribute->move_speed = 1.5f;
  attribute->curr_anim = nullptr;
  attribute->hit_box = create_rectangle(v2_0, 24.0f, 36.0f);
  attribute->is_die = false;
  attribute->texture_opacity = 255.0f;
  attribute->grid_x = 0;
  attribute->grid_y = 0;
  return attribute;
}

SpriteAnimator* get_sprite_animator(Node* node){
  return (SpriteAnimator*)node->component[0];
}

Attribute* get_attribute(Node* node){
  return (Attribute*)node->component[1];
}

ResourceManager *resources_manager;

Node *scene;
Node *hero;
Node *momon;
const int momon_size = 30;
Rectangle *screen_rect;

Texture *hero_texture;
Sprite *hero_sprite;
Texture *momon_texture;
Sprite *momon_sprite;
Texture *dia_red_texture;
Texture *dia_green_texture;
Texture *dia_yellow_texture;
Node *hero_node_front;
Node *hero_node_right;
Node *hero_node_left;

Tiledmap *map1;
int **collision_map;

int down_anim[4] = {1, 2, 3, 2};
int up_anim[4] = {10, 11, 12, 11};
int right_anim[4] = {7, 8, 9, 8};
int left_anim[4] = {4, 5, 6, 5};
int die_anim[5] = {2, 5, 11, 8};

float world_width = 1600.0f;
float world_height = 1600.0f;
float hero_width = 48.0f;
float hero_height = 48.0f;

KeyButton *right_key;
KeyButton *left_key;
KeyButton *up_key;
KeyButton *down_key;

fun::vector<Node*> character_list;

std::random_device random_device;
std::default_random_engine random_engine(random_device());

Node* instantiate_character(Node *node, const char *name){
  Node *inst_node = create_node(name);
  inst_node->tag = node->tag;
  Sprite *sprite = (Sprite*)malloc(sizeof(Sprite));
  SpriteAnimator *sprite_animator = (SpriteAnimator*)malloc(sizeof(SpriteAnimator));
  Attribute *attribute = (Attribute*)malloc(sizeof(Attribute));

  memcpy(sprite, get_sprite_animator(node)->sprite, sizeof(Sprite));
  memcpy(sprite_animator, get_sprite_animator(node), sizeof(SpriteAnimator));
  memcpy(attribute, get_attribute(node), sizeof(Attribute));

  sprite_animator->sprite = sprite;

  inst_node->component.push_back(sprite_animator);
  inst_node->component.push_back(attribute);

  Rectangle *hit_box =
    create_rectangle(v2_0,
		     get_attribute(node)->hit_box->width,
		     get_attribute(node)->hit_box->height);

  get_attribute(inst_node)->hit_box = hit_box;

  return inst_node;
}

void free_character(Node *node){
  // free(get_sprite_animator(node));
  // free(get_attribute(node));
  free(node);
}

void random_move(Node *node){

  float x = node->transform->position.x; 
  float y = node->transform->position.y; 

  bool is_outside = !is_intersect(screen_rect, node->transform->position);

  float rand_r = 35;

  if(is_outside){

    int inverse_direction = 0;

    bool is_right_collide = x > logical_screen_width;
    bool is_left_collide = x < 0;
    bool is_up_collide = y < 0;
    bool is_down_collide = y > logical_screen_height;

    if(is_right_collide){
      inverse_direction = 270;
    }
    else if(is_left_collide){
      inverse_direction = 90;
    }

    if(is_down_collide){
      inverse_direction = 360;
    }
    else if(is_up_collide){
      inverse_direction = 180;
    }

    int min_rand_angle = inverse_direction - rand_r;
    int max_rand_angle = inverse_direction + rand_r;
    std::uniform_int_distribution<int> int_dist(min_rand_angle, max_rand_angle);
    int n_move_angle = int_dist(random_engine) % 360; 
    
    get_attribute(node)->move_angle = n_move_angle;
  }

  node->transform->position +=
    move_toward(get_attribute(node)->move_angle, get_attribute(node)->move_speed);

}

void on_init(){
  resources_manager = create_resource_manager("./resources.lua");

  hero_texture = load_texture(renderer, resources_manager->get_path("chara1"));
  hero_sprite = create_sprite(hero_texture);
  SpriteAnimator *hero_sprite_animator = create_sprite_animator(hero_sprite, v2i{48, 48});

  dia_red_texture = load_texture(renderer, resources_manager->get_path("dia_red_24"));
  dia_green_texture = load_texture(renderer, resources_manager->get_path("dia_green_24"));
  dia_yellow_texture = load_texture(renderer, resources_manager->get_path("dia_yellow_24"));

  momon_texture = load_texture(renderer, resources_manager->get_path("piggy"));
  momon_sprite = create_sprite(momon_texture);
  SpriteAnimator *momon_sprite_animator = create_sprite_animator(momon_sprite, v2i{48, 48});

  Attribute *hero_attribute = create_attribute();
  Attribute *momon_attribute = create_attribute();

  right_key = create_key_button("right", SDLK_d);
  left_key = create_key_button("left", SDLK_a);
  up_key = create_key_button("up", SDLK_w);
  down_key = create_key_button("down", SDLK_s);

  add_key_button(right_key);
  add_key_button(left_key);
  add_key_button(up_key);
  add_key_button(down_key);

  scene = create_node("main_scene");
  screen_rect = create_rectangle(v2{(float)logical_screen_width / 2, (float)logical_screen_height / 2},
				 logical_screen_width,
				 logical_screen_height
				 );

  hero = create_node("hero");
  hero->tag = "hero";
  hero_attribute->curr_anim = down_anim;
  hero->component.push_back((void*)hero_sprite_animator);
  hero->component.push_back((void*)hero_attribute);
  hero->transform->position = v2{(float)(logical_screen_width / 2.0f), 50.0f};
  get_attribute(hero)->hit_box->position = hero->transform->position;
  get_attribute(hero)->move_speed = 2.0f;
  get_attribute(hero)->move_angle = 180.0f;

  hero_node_front = create_node("dia red");
  hero_node_front->transform->rotation = 180.0f;
  hero_node_right = create_node("dia green");
  hero_node_left = create_node("dia yellow");

  momon = create_node("momon");
  momon->tag = "momon";
  momon_attribute->curr_anim = down_anim;
  momon->component.push_back((void*)momon_sprite_animator);
  momon->component.push_back((void*)momon_attribute);

  character_list = create_vector<Node*>();
  character_list.push_back(hero);

  for(int i = 0; i < momon_size; ++i){
    fun::string name = create_string("momon");
    name += std::to_string(i);
    Node *momon_inst = instantiate_character(momon, name.c_str);

    int index = i % 5;

    if(i < 5){
      get_attribute(momon_inst)->move_angle = 90.0f;
      momon_inst->transform->position.x = 165;
      momon_inst->transform->position.y = index + (100 * (index + 1) );
    }
    else if(i >= 5 && i < 10){
      get_attribute(momon_inst)->move_angle = 360.0f;
      momon_inst->transform->position.x = index + (100 * (index + 1) );
      momon_inst->transform->position.y = logical_screen_height - 165;
    }
    else if(i >= 10 && i < 15){
      get_attribute(momon_inst)->move_angle = 180.0f;
      momon_inst->transform->position.x = index + (100 * (index + 1) );
      momon_inst->transform->position.y = 65;
    }
    else if(i >= 15 && i < 20){
      get_attribute(momon_inst)->move_angle = 360.0f;
      momon_inst->transform->position.x = index + (100 * (index + 1) );
      momon_inst->transform->position.y = logical_screen_height - 65;
    }
    else if(i >= 20 && i < 25){
      get_attribute(momon_inst)->move_angle = 270.0f;
      momon_inst->transform->position.x = logical_screen_width - 65;
      momon_inst->transform->position.y = index + (100 * (index + 1) );
    }
    else if(i >= 25 && i < 30){
      get_attribute(momon_inst)->move_angle = 90.0f;
      momon_inst->transform->position.x = 65;
      momon_inst->transform->position.y = index + (100 * (index + 1) );
    }

    get_attribute(momon_inst)->hit_box->position = momon_inst->transform->position;

    character_list.push_back(momon_inst);
  }

  map1 = create_tiledmap(renderer, resources_manager->get_path("map1"));
  collision_map = map1->layer_list[1]->map_arr;

}

void controller(){

   float move_speed = get_attribute(hero)->move_speed;
   float accel = get_attribute(hero)->accel;

   if(right_key->is_pressed){
     get_attribute(hero)->move_angle = 90.0f;

     if(get_attribute(hero)->horizontal_state == -1){
       get_attribute(hero)->hsp = 0.0f;
     }
     else{
       if(get_attribute(hero)->hsp < move_speed){
	 get_attribute(hero)->hsp += accel;
       }
     }

     get_attribute(hero)->horizontal_state = 1;

   }

   if(left_key->is_pressed){
     get_attribute(hero)->move_angle = 270.0f;

     if(get_attribute(hero)->horizontal_state == 1){
       get_attribute(hero)->hsp = 0.0f;
     }
     else{
       if(get_attribute(hero)->hsp > -move_speed){
	 get_attribute(hero)->hsp -= accel;
       }
     }

     get_attribute(hero)->horizontal_state = -1;

   }

   if(!right_key->is_pressed &&
      !left_key->is_pressed){

     get_attribute(hero)->horizontal_state = 0;
     get_attribute(hero)->hsp = 0.0f;
   }

   if(down_key->is_pressed){
     get_attribute(hero)->move_angle = 180.0f;

     if(get_attribute(hero)->vertical_state == -1){
       get_attribute(hero)->vsp = 0.0f;
     }
     else{
       if(get_attribute(hero)->vsp < move_speed){
	 get_attribute(hero)->vsp += accel;
       }
     }

     get_attribute(hero)->vertical_state = 1;

   }

   if(up_key->is_pressed){
     get_attribute(hero)->move_angle = 360.0f;

     if(get_attribute(hero)->vertical_state == 1){
       get_attribute(hero)->vsp = 0.0f;
     }
     else{
       if(get_attribute(hero)->vsp > -move_speed){
	 get_attribute(hero)->vsp -= accel;
       }
     }

     get_attribute(hero)->vertical_state = -1;
   }


   if(!up_key->is_pressed &&
      !down_key->is_pressed){

     get_attribute(hero)->vsp = 0.0f;

   }

   v2 start_vel_position = v2{0.0f, -get_attribute(hero)->vsp};
   v2 target_vel_position = v2{get_attribute(hero)->hsp, 0.0f};

   float azimuth = 0.0f; 
   float n_move_speed = move_speed;

   if(get_attribute(hero)->hsp != 0 ||
      get_attribute(hero)->vsp != 0){

     azimuth = get_azimuth(start_vel_position, target_vel_position);
     hero_node_front->transform->rotation = azimuth;
     hero_node_right->transform->rotation = azimuth;
     hero_node_left->transform->rotation = azimuth;
   }
   else{
     n_move_speed = 0.0f;
   }

   float r = 30.0f;

   hero->transform->position +=
     move_toward(azimuth, n_move_speed);

   hero_node_front->transform->position =
     get_parametric(hero->transform->position,
		    hero_node_front->transform->rotation,
		    r);
   hero_node_right->transform->position =
     get_parametric(hero->transform->position,
		    hero_node_front->transform->rotation + 90.0f,
		    r);

   hero_node_left->transform->position =
     get_parametric(hero->transform->position,
		    ((int)hero_node_front->transform->rotation - 90) % 360,
		    r);

}

void character_list_bubble_sort(){

  for(int i = 0; i < character_list.size; ++i){

    int index = i;
    float y = character_list[i]->transform->position.y;

    Node *node = character_list[i];

    for(int j = i + 1; j < character_list.size; ++j){

      if(y > character_list[j]->transform->position.y){
	y = character_list[j]->transform->position.y;
	index = j;
      }
    } // j

    Node* swap_node = character_list[index];

    character_list.t[i] = swap_node;
    character_list.t[index] = node;

  } // i

}

void on_update(){
  controller();
  character_list_bubble_sort();

  for(int i = 0; i < character_list.size; ++i){

    Node *node = character_list[i];
    float move_angle = get_attribute(node)->move_angle;

    get_attribute(node)->grid_x = node->transform->position.x / 48;
    get_attribute(node)->grid_y = node->transform->position.y / 48;

    float dir_r = 45;
    if(move_angle >= 90 - dir_r &&
       move_angle <= 90 + dir_r){
      get_attribute(node)->dir_state = right_dir;
    }

    if(move_angle >= 270 - dir_r &&
       move_angle <= 270 + dir_r){
      get_attribute(node)->dir_state = left_dir;
    }

    if(move_angle > 180 - dir_r &&
       move_angle < 180 + dir_r){
      get_attribute(node)->dir_state = down_dir;
    }

    if(move_angle > 360 -dir_r && move_angle <= 360 ||
       move_angle >=0 && move_angle < dir_r){
      get_attribute(node)->dir_state = up_dir;
    }

    if(!get_attribute(node)->is_die)
      
      switch(get_attribute(node)->dir_state){
      case right_dir:
	get_attribute(node)->curr_anim = right_anim;
	break;
      case left_dir:
	get_attribute(node)->curr_anim = left_anim;
	break;
      case up_dir:
	get_attribute(node)->curr_anim = up_anim;
	break;
      case down_dir:
	get_attribute(node)->curr_anim = down_anim;
	break;
      }

    SpriteAnimator *sprite_animator = get_sprite_animator(node);
    Attribute *attr = get_attribute(node);

    if(!get_attribute(node)->is_die){
      run_sprite_animator(sprite_animator, attr->curr_anim, 4, 5);
    }
    else{
      run_sprite_animator(sprite_animator, attr->curr_anim, 4, 10);
    }

    get_attribute(node)->hit_box->position = node->transform->position;

    if(node->tag == "momon"){
      if(!get_attribute(node)->is_die){
	random_move(node);
      }
    }

    // die anim
    if(get_attribute(node)->is_die == true){

      float scale_speed = 0.07;

      if(node->transform->scale.x < 1.5f){
	node->transform->scale.x += scale_speed;
      }

      if(node->transform->scale.y < 1.5f){
	node->transform->scale.y += scale_speed;
      }

      if(!get_sprite_animator(node)->sprite->is_visible){
	character_list.erase(character_list.find(node));
      }

    }

  }

  // hit test
  if(get_attribute(hero)->hsp != 0 ||
     get_attribute(hero)->vsp != 0){

    for(int i = 0; i < character_list.size; ++i){

       Node *node = character_list[i];


       // if(node->tag == "momon"){
       // 	 if(is_intersect(get_attribute(node)->hit_box, get_attribute(hero)->hit_box)){
       // 	   get_attribute(node)->move_angle = get_attribute(hero)->move_angle;
       // 	 }
       // }

      if(node->tag == "momon"){

      	if(is_intersect(get_attribute(hero)->hit_box, get_attribute(node)->hit_box )){
      	  get_attribute(node)->is_die = true;
      	  get_attribute(node)->curr_anim = die_anim;
      	}
      }
    }
  }
}

void on_draw(){
  set_renderer_background_color(renderer, SKYBLUE);

  draw_tiledmap(renderer, map1, 24.0f, 24.0f);

  for(int i = 0; i < character_list.size; ++i){
    Node *node = character_list[i];
    SpriteAnimator *sprite_animator = get_sprite_animator(node);

    // draw_rectangle(renderer,
    // 		  get_attribute(node)->hit_box,
    // 		  DARKBLUE);

    float opacity_speed = 7.0f;
    if(get_attribute(node)->is_die == true){

      if(get_attribute(node)->texture_opacity > 0.0f){

      	if(get_attribute(node)->texture_opacity - opacity_speed > 0.0f){
      	  get_attribute(node)->texture_opacity -= opacity_speed;
      	}
      	else{
      	  get_attribute(node)->texture_opacity = 0.0f;
      	}

      }
      else{
      	sprite_animator->sprite->is_visible = false;
      }

      texture_mod_alpha(sprite_animator->sprite->texture,
      			get_attribute(node)->texture_opacity);
    }

    draw_sprite_animator(renderer,
			 sprite_animator,
			 node->transform);

    texture_mod_alpha(sprite_animator->sprite->texture, 255.0f);


  }

  draw_texture(renderer,
	       dia_red_texture,
	       24,
	       24,
	       hero_node_front->transform->position.x,
	       hero_node_front->transform->position.y);

  draw_texture(renderer,
	       dia_green_texture,
	       24,
	       24,
	       hero_node_right->transform->position.x,
	       hero_node_right->transform->position.y);

  draw_texture(renderer,
	       dia_yellow_texture,
	       24,
	       24,
	       hero_node_left->transform->position.x,
	       hero_node_left->transform->position.y);

}

void on_draw_gui(){
}

void on_clear(){
}

int main(){
  create_app(800, 800, "Dungeon Of...", true, "nearest");
  on_init();

  run_app(on_update, 
	  on_draw,
	  on_draw_gui,
	  60);

  on_clear();
  clear_app();
  SDL_Quit();

  return 0;
}
